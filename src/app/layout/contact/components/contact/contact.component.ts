import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  public contact: any;
  
  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    // Gallery
    this.translate.stream('Contact').subscribe((res) => {
      this.contact = res;
    });
  }

}
