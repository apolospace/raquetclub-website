import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public about: any;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    // About
    this.translate.stream('About').subscribe((res) => {
      this.about = res;
    });
  }

}
