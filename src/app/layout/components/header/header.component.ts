import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('stickyMenu', { static: false }) menuElement: ElementRef

  public menuOptions: Array<object>;
  public checkMenu: boolean = false;
  public sticky: boolean = false;
  public elementPosition: any;
  public size = window.matchMedia("(max-width: 768px)");

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    // Change Language with Stream after loading.
    this.translate.stream('Header').subscribe((res) => {
      this.menuOptions = res;
    });

    const lang = localStorage.getItem('lang');

    this.changeLang(lang);
  }

  ngAfterViewInit() {
    this.elementPosition = this.menuElement.nativeElement.offsetTop;
  }

  changeLang(lang: string) {
    this.translate.use(lang);
    localStorage.setItem('lang', lang);
  }

  openMenu(event: Event) {
    event.preventDefault();

    if (this.size.matches) { // If media query matches
      this.checkMenu = !this.checkMenu;
    }
  }

  @HostListener('window:scroll', ['$event'])
  handleScroll() {

    const windowScroll = window.pageYOffset;

    if (windowScroll >= 171) {
      this.sticky = true;


    } else {
      this.sticky = false;
    }
  }

}
