import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @ViewChild('stickyMenu', { static: false }) menuElement: ElementRef;

  sticky: boolean = false;
  elementPosition: any;

  public menuOptions: any;
  public footer: any;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    // Change Language with Stream after loading.
    this.translate.stream('Header').subscribe((res) => {
      this.menuOptions = res;
    });

    this.translate.stream('Footer').subscribe((res) => {
      this.footer = res;
    });
  }

}
