import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  public gallery: any;
  
  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    // Gallery
    this.translate.stream('Gallery').subscribe((res) => {
      this.gallery = res;
    });
  }

}
